// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
//
// Description: This program returns the median of a template.
//===========================================================================
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;

template <typename T>
T median(vector<T> v){
	T med;
	//Sorts the vector to use the median efectively
	sort(v.begin(),v.end());
	int size = v.size();
	if (size % 2 == 1){
		//return the position in the half
		return v[size /2];
	}
	else
		//returns the the average of the two in the middle
		return ( (v[size/2] + v[(size/2) - 1]) /2);
}
int main(){
int A[] = {15, 1, 2, 7, 17};
vector<int> V(A, A+5);
  cout << median(V) << endl;  // imprime 7

double B[] = {15.1, 1.4, 2.3, 7.12, 17.99};
vector<float> W(B, B+5);
cout << median(W) << endl;    // imprime 7.12
	return 0;
}