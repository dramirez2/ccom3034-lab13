// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
//
// Description: This program overloads the operator > to check which element 
// of a template myPair is greater.
//===========================================================================
#include <iostream>
#include <cstdlib>
using namespace std;


template <typename Ta, typename Tb>
class myPair {
private:
  Ta a;
  Tb b;
public:
  myPair();
  myPair(Ta, Tb);
  Ta getFirst();
  Tb getSecond();
   bool operator>( myPair& comp);
};

template <typename Ta, typename Tb>
myPair<Ta,Tb>::myPair() { /* do nothing */ }

template <typename Ta, typename Tb>
myPair<Ta,Tb>::myPair(Ta va, Tb vb) { a = va; b = vb; }

template <typename Ta, typename Tb>
Ta myPair<Ta,Tb>::getFirst()  { return a; }

template <typename Ta, typename Tb>
Tb myPair<Ta,Tb>::getSecond() { return b; }

template<typename Ta, typename Tb>
bool myPair<Ta,Tb>::operator>(myPair & A){
	if( a > A.getFirst() )
		return true;
	else
		return false;
}
int main(){
	myPair<int,string> P(8, "ocho");
	myPair<int,string> Q(10, "diez");
	cout << "Q is larger than P: " << std::boolalpha 
	     << (Q > P) << endl;  // should print "true"

	return 0;
}