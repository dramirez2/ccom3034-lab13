// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
//
// Description: This program implements a function to the template list, which 
// returns the element in the inserted position 
//===========================================================================
#include <iostream>
#include <cstdlib>
using namespace std;

template <typename T>
class List {
private:
  T *myArray;
  int mySize, myCapacity;

public: 
  List(); // the constructor  
  void insert(T element, int pos);
  void print();
  T at(int pos);
  // . . . 
};


template <typename T>
List<T>::List(){
  myArray = new T[10];
  mySize = 0;
  myCapacity = 10;
}

template <typename T>
void List<T>::insert(T element, int pos) {
  if (pos < 0 || pos > mySize || pos >= myCapacity  )
    return;
  for(int i=mySize; i>pos; i--)
    myArray[i] = myArray[i-1];
  myArray[pos] = element;
  mySize++;
}

template <typename T>
void List<T>::print() {
  for (int i=0; i<mySize;i++) 
      cout << myArray[i] << " ";
  cout << endl;
}

//Returns the element on the position
template <typename T>
T List<T>::at(int pos){
  //Creates an empty template element to return if it is empty
  //or an invalid position
  T nothing;

  if (mySize == 0){
    cout << "The list is empty." << endl;
    return nothing;
  }

 else if (pos < 0 || pos >= myCapacity || pos > mySize){
    cout << "Attempting to insert at an invalid postion." << endl;
    return nothing;
  }
  //Proceed normally if no error found from the position
  else {
    return myArray[pos]; 
  }

}
int main(){
  int num;
  List<string> L;
  L.insert("roble",0);
  L.insert("ucar",0);
  L.insert("ficus",2);
  cin >> num;
  cout << L.at(num) << endl;  // prints "roble"
  return 0;
}