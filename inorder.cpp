// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
//
// Description: This program verifies if the elements in a template array are
// or aren't in order
//===========================================================================
#include <iostream>
using namespace std;

template <typename T>
bool inOrder(T *array, int size){
	for (int i=1; i<size; i++){
		if(array[i]<array[i-1])
			return false;
	}
	return true;
}

int main(){
	int A[] = {15, 1, 2, 7, 17};
	int C[] = {1, 1, 2, 7, 17};
	cout << "inorder A:" << std::boolalpha << inOrder(A,5) << endl;
	cout << "inorder C:" << inOrder(C,5) << endl;
	
	double B[] = {15.1, 1.4, 2.3, 7.12, 17.99};
	cout << "inorder B:" <<   inOrder(B,5) << endl;   
	
	string S[] = {"arroz", "habichuelas", "jamon" , "queso", "zetas"};
	cout << "inorder S:" << inOrder(S,5) << endl;
	
	return 0;
}
